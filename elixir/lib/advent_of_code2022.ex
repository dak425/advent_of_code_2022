defmodule AdventOfCode2022 do
  alias AdventOfCode2022.{Calories, RockPaperScissors}

  def solve_day_1_part_1(input_file_path) do
    input_file_path
    |> Calories.parse_from_file()
    |> Calories.sum_calories()
    |> Enum.max()
  end

  def solve_day_1_part_2(input_file_path) do
    input_file_path
    |> Calories.parse_from_file()
    |> Calories.sum_calories()
    |> Enum.sort(:desc)
    |> Enum.take(3)
    |> Enum.sum()
  end

  def solve_day_2_part_1(input_file_path) do
    input_file_path
    |> RockPaperScissors.parse_moves_from_file()
    |> RockPaperScissors.predict_score_from_moves()
  end

  def solve_day_2_part_2(input_file_path) do
    input_file_path
    |> RockPaperScissors.parse_moves_from_file()
    |> RockPaperScissors.predict_score_from_moves_correctly()
  end
end
