defmodule AdventOfCode2022.RockPaperScissors do
  @rock_value 1
  @paper_value 2
  @scissors_value 3
  @lose_game_value 0
  @draw_game_value 3
  @win_game_value 6

  def parse_moves_from_file(path) do
    path
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, " "))
  end

  def predict_score_from_moves(moves) do
    moves
    |> Enum.map(&score_game/1)
    |> Enum.sum()
  end

  def predict_score_from_moves_correctly(moves) do
    moves
    |> score_game_correctly()
  end

  defp score_game(["A", "X"]), do: @draw_game_value + @rock_value
  defp score_game(["A", "Y"]), do: @win_game_value + @paper_value
  defp score_game(["A", "Z"]), do: @lose_game_value + @scissors_value
  defp score_game(["B", "X"]), do: @lose_game_value + @rock_value
  defp score_game(["B", "Y"]), do: @draw_game_value + @paper_value
  defp score_game(["B", "Z"]), do: @win_game_value + @scissors_value
  defp score_game(["C", "X"]), do: @win_game_value + @rock_value
  defp score_game(["C", "Y"]), do: @lose_game_value + @paper_value
  defp score_game(["C", "Z"]), do: @draw_game_value + @scissors_value

  defp score_game_correctly([]), do: 0

  defp score_game_correctly([["A", "X"] | remaining_moves]) do
    @lose_game_value + @scissors_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["A", "Y"] | remaining_moves]) do
    @draw_game_value + @rock_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["A", "Z"] | remaining_moves]) do
    @win_game_value + @paper_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["B", "X"] | remaining_moves]) do
    @lose_game_value + @rock_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["B", "Y"] | remaining_moves]) do
    @draw_game_value + @paper_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["B", "Z"] | remaining_moves]) do
    @win_game_value + @scissors_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["C", "X"] | remaining_moves]) do
    @lose_game_value + @paper_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["C", "Y"] | remaining_moves]) do
    @draw_game_value + @scissors_value + score_game_correctly(remaining_moves)
  end

  defp score_game_correctly([["C", "Z"] | remaining_moves]) do
    @win_game_value + @rock_value + score_game_correctly(remaining_moves)
  end
end
