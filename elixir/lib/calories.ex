defmodule AdventOfCode2022.Calories do
  def parse_from_file(path) do
    File.read!(path)
    |> String.split("\n\n")
    |> Enum.map(&String.split(&1, "\n", trim: true))
    |> Enum.map(&convert_calories_to_int/1)
  end

  def sum_calories(calories) when is_list(calories) do
    calories
    |> Enum.map(&Enum.sum/1)
  end

  defp convert_calories_to_int(calories) do
    calories
    |> Enum.map(&String.to_integer/1)
  end
end
